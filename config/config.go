package config

import (
	"time"
)

const (
	DefaultNatsURL            = "nats://nats:4222"
	DefaultNatsClusterID      = "cacao-cluster"
	DefaultNatsMaxReconnect   = 6                // max times to reconnect within nats.connect()
	DefaultNatsReconnectWait  = 10 * time.Second // seconds to wait within nats.connect()
	DefaultNatsRequestTimeout = 5 * time.Second  // timeout for requests

	DefaultMongoDBURL  = "mongodb://localhost:27017"
	DefaultMongoDBName = "cacao"

	DefaultStanEventsSubject = "cyverse.events"
	DefaultStanEventsTimeout = 10 * time.Second // used to wait for event ops to complete, is very conservation
)

// NatsConfig stores configurations used by both nats query channel and nats streaming
type NatsConfig struct {
	URL             string `envconfig:"NATS_URL" default:"nats://nats:4222"`
	QueueGroup      string `envconfig:"NATS_QUEUE_GROUP"`
	WildcardSubject string `envconfig:"NATS_WILDCARD_SUBJECT" default:"cyverse.>"` // WildcardSubject field is optional, only used for NATS Query
	ClientID        string `envconfig:"NATS_CLIENT_ID"`                            // While not strictly used by Nats, this is propagated into the cloudevent
	MaxReconnects   int    `envconfig:"NATS_MAX_RECONNECTS" default:"-1"`          // implementation should default to DefaultNatsMaxReconnect
	ReconnectWait   int    `envconfig:"NATS_RECONNECT_WAIT" default:"-1"`          // in seconds, implementation should default to
	RequestTimeout  int    `envconfig:"NATS_REQUEST_TIMEOUT" default:"-1"`
}

// StanConfig stores additional configurations used by nats streaming, used along with NatsConfig DefaultNatsReconnectWait
type StanConfig struct {
	ClusterID     string `envconfig:"NATS_CLUSTER_ID" default:"cacao-cluster"`
	DurableName   string `envconfig:"NATS_DURABLE_NAME"`
	EventsTimeout int    `envconfig:"NATS_EVENTS_TIMEOUT" default:"-1"`
}

// MongoDBConfig stores configurations used by mongodb
type MongoDBConfig struct {
	URL    string `envconfig:"MONGODB_URL" default:"mongodb://localhost:27017"`
	DBName string `envconfig:"MONGODB_DB_NAME" default:"cacao"`
}
